import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InterfaceComponentsPage } from './interface-components';

@NgModule({
  declarations: [
    InterfaceComponentsPage,
  ],
  imports: [
    IonicPageModule.forChild(InterfaceComponentsPage),
  ],
})
export class InterfaceComponentsPageModule {}
